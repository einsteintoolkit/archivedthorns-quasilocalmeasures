# 0D ASCII output created by CarpetIOASCII
#
# QUASILOCALMEASURES::QLM_MULTIPOLE_MOMENTS (quasilocalmeasures::qlm_multipole_moments)
#
# iteration 0
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
0	0 0 0 0	0 0 0	0	0 0 0	-0.784748116784676 2.78908349595892e-16 0.394518852272564 -2.38694300545776e-15 0.200525094392784 -2.51479187906703e-14 0.256817074507714 -4.46942924460937e-14 0.463659628512012 3.49867502913406e-32 2.92548163185694e-33 -3.42779878525142e-32 -6.95576864838405e-32 2.05214298392255e-31 2.64515247946309e-31 -5.76523628267489e-31 -7.37726866517123e-32 -1.12056127386731e-30

# iteration 1
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
1	0 0 0 0	0 0 0	0.05	0 0 0	-0.784748116784676 2.78908349595892e-16 0.394518852272564 -2.38694300545776e-15 0.200525094392784 -2.51479187906703e-14 0.256817074507714 -4.46942924460937e-14 0.463659628512012 3.49867502913406e-32 2.92548163185694e-33 -3.42779878525142e-32 -6.95576864838405e-32 2.05214298392255e-31 2.64515247946309e-31 -5.76523628267489e-31 -7.37726866517123e-32 -1.12056127386731e-30

# iteration 2
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
2	0 0 0 0	0 0 0	0.1	0 0 0	-0.784748116784676 2.78908349595892e-16 0.394518852272564 -2.38694300545776e-15 0.200525094392784 -2.51479187906703e-14 0.256817074507714 -4.46942924460937e-14 0.463659628512012 3.49867502913406e-32 2.92548163185694e-33 -3.42779878525142e-32 -6.95576864838405e-32 2.05214298392255e-31 2.64515247946309e-31 -5.76523628267489e-31 -7.37726866517123e-32 -1.12056127386731e-30

# iteration 3
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
3	0 0 0 0	0 0 0	0.15	0 0 0	-0.784748116784676 2.78908349595892e-16 0.394518852272564 -2.38694300545776e-15 0.200525094392784 -2.51479187906703e-14 0.256817074507714 -4.46942924460937e-14 0.463659628512012 3.49867502913406e-32 2.92548163185694e-33 -3.42779878525142e-32 -6.95576864838405e-32 2.05214298392255e-31 2.64515247946309e-31 -5.76523628267489e-31 -7.37726866517123e-32 -1.12056127386731e-30

