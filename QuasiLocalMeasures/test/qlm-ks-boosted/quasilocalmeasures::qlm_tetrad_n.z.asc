# 1D ASCII output created by CarpetIOASCII
#
# QUASILOCALMEASURES::QLM_TETRAD_N z (quasilocalmeasures::qlm_tetrad_n)
#
# iteration 0
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_n0[0] 14:qlm_n1[0] 15:qlm_n2[0] 16:qlm_n3[0]
0	0 0 0 0	0 0 0	0	0 0 0	0 0.0411488153135142 0.0121262111571826 -0.590798977414101


# iteration 1
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_n0[0] 14:qlm_n1[0] 15:qlm_n2[0] 16:qlm_n3[0]
1	0 0 0 0	0 0 0	0.05	0 0 0	0 0.0417114128460514 0.0122887443240348 -0.590776726564161


# iteration 2
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_n0[0] 14:qlm_n1[0] 15:qlm_n2[0] 16:qlm_n3[0]
2	0 0 0 0	0 0 0	0.1	0 0 0	0 0.0422741184237497 0.0124509773950367 -0.59075442362748


# iteration 3
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_n0[0] 14:qlm_n1[0] 15:qlm_n2[0] 16:qlm_n3[0]
3	0 0 0 0	0 0 0	0.15	0 0 0	0 0.0428369050709207 0.0126131152888088 -0.590731752060661


