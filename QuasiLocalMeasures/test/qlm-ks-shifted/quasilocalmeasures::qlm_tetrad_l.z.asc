# 1D ASCII output created by CarpetIOASCII
#
# QUASILOCALMEASURES::QLM_TETRAD_L z (quasilocalmeasures::qlm_tetrad_l)
#
# iteration 0
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_l0[0] 14:qlm_l1[0] 15:qlm_l2[0] 16:qlm_l3[0]
0	0 0 0 0	0 0 0	0	0 0 0	0 -0.0577550213998325 -0.017108615003116 0.497448478670079


# iteration 1
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_l0[0] 14:qlm_l1[0] 15:qlm_l2[0] 16:qlm_l3[0]
1	0 0 0 0	0 0 0	0.05	0 0 0	0 -0.0584743613052848 -0.017319850727987 0.497384406240289


# iteration 2
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_l0[0] 14:qlm_l1[0] 15:qlm_l2[0] 16:qlm_l3[0]
2	0 0 0 0	0 0 0	0.1	0 0 0	0 -0.0591937300594253 -0.0175309309017488 0.497319545493369


# iteration 3
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_l0[0] 14:qlm_l1[0] 15:qlm_l2[0] 16:qlm_l3[0]
3	0 0 0 0	0 0 0	0.15	0 0 0	0 -0.0599130741148403 -0.0177419418855416 0.497253844480518


