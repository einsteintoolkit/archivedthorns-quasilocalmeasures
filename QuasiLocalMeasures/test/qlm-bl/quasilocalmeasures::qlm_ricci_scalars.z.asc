# 1D ASCII output created by CarpetIOASCII
#
# QUASILOCALMEASURES::QLM_RICCI_SCALARS z (quasilocalmeasures::qlm_ricci_scalars)
#
# iteration 0
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_phi00[0] 14:qlm_phi11[0] 15:qlm_phi01[0] 16:qlm_phi12[0] 17:qlm_phi10[0] 18:qlm_phi21[0] 19:qlm_phi02[0] 20:qlm_phi22[0] 21:qlm_phi20[0] 22:qlm_lambda[0] 23:qlm_lie_n_theta_l[0]
0	0 0 0 0	0 0 0	0	0 0 0	-3.80155757996001e-17 1.17819440104748e-17 8.93536793113614e-19 -8.93536793113614e-19 8.93536793113614e-19 -8.93536793113614e-19 -9.05746195326108e-20 -3.80155757996001e-17 -9.05746195326108e-20 0.00021474922823586 -0.123770004359622


# iteration 1
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_phi00[0] 14:qlm_phi11[0] 15:qlm_phi01[0] 16:qlm_phi12[0] 17:qlm_phi10[0] 18:qlm_phi21[0] 19:qlm_phi02[0] 20:qlm_phi22[0] 21:qlm_phi20[0] 22:qlm_lambda[0] 23:qlm_lie_n_theta_l[0]
1	0 0 0 0	0 0 0	0.0125	0 0 0	-1.29498585623767e-17 4.59074192194609e-17 -1.4978743757247e-19 1.4978743757247e-19 -1.4978743757247e-19 1.4978743757247e-19 -1.95330845171434e-17 -1.29498585623767e-17 -1.95330845171434e-17 6.44291769560485e-05 -0.124693784539511


# iteration 2
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_phi00[0] 14:qlm_phi11[0] 15:qlm_phi01[0] 16:qlm_phi12[0] 17:qlm_phi10[0] 18:qlm_phi21[0] 19:qlm_phi02[0] 20:qlm_phi22[0] 21:qlm_phi20[0] 22:qlm_lambda[0] 23:qlm_lie_n_theta_l[0]
2	0 0 0 0	0 0 0	0.025	0 0 0	-4.99045732540179e-17 7.67612583203652e-17 -4.036837973048e-18 4.036837973048e-18 -4.036837973048e-18 4.036837973048e-18 1.02563315979747e-17 -4.99045732540179e-17 1.02563315979747e-17 -9.45307752651434e-05 -0.125666199700211


# iteration 3
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_phi00[0] 14:qlm_phi11[0] 15:qlm_phi01[0] 16:qlm_phi12[0] 17:qlm_phi10[0] 18:qlm_phi21[0] 19:qlm_phi02[0] 20:qlm_phi22[0] 21:qlm_phi20[0] 22:qlm_lambda[0] 23:qlm_lie_n_theta_l[0]
3	0 0 0 0	0 0 0	0.0375	0 0 0	4.83059342854448e-17 -1.6111870436613e-17 1.73300786955519e-18 -1.73300786955519e-18 1.73300786955519e-18 -1.73300786955519e-18 -8.12700589447137e-17 4.83059342854448e-17 -8.12700589447137e-17 -0.00025595010685192 -0.126649322702421


