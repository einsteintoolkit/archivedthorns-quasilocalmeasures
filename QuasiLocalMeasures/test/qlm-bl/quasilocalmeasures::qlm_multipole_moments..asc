# 0D ASCII output created by CarpetIOASCII
#
# QUASILOCALMEASURES::QLM_MULTIPOLE_MOMENTS (quasilocalmeasures::qlm_multipole_moments)
#
# iteration 0
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
0	0 0 0 0	0 0 0	0	0 0 0	-0.784828508659574 -1.255712956211e-16 0.394436084160111 -3.21474695930373e-15 0.201306431766946 4.02352529746489e-15 0.259808534981209 -1.49019520294235e-14 0.44679273449799 -1.00175948330903e-18 6.20259232503916e-19 1.21131855764936e-18 -3.06716013637578e-18 -1.87595857037115e-18 5.03086118113278e-18 -1.39657589782169e-17 1.67284370583202e-17 9.60366332720873e-17

# iteration 1
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
1	0 0 0 0	0 0 0	0.0125	0 0 0	-0.784751412070078 -3.09445708890425e-16 0.394397481164478 -1.09395051733656e-14 0.201155862158382 -7.90460997296961e-15 0.259092593756262 -3.52879179628955e-14 0.454282145411038 -2.31195445681771e-18 3.60558649209607e-19 2.86212362578792e-18 1.66190623520164e-18 -2.53753939351799e-18 -1.02454630323958e-17 -3.92228636775725e-18 -2.3771755349801e-18 5.24000706685883e-17

# iteration 2
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
2	0 0 0 0	0 0 0	0.025	0 0 0	-0.784825525632908 -3.1108922320704e-16 0.394304761868794 1.82982646031597e-15 0.202056448690427 -4.08844052307921e-15 0.257637443268979 -5.73047220334937e-14 0.451738207821442 1.13373759893368e-18 -1.83040197459765e-18 -4.12859538056353e-18 3.40850514534886e-19 1.02779216695185e-17 2.29591433870118e-17 2.61552553700365e-17 -3.92947551166297e-17 -1.85428537455816e-16

# iteration 3
# refinement level 0   multigrid level 0   map 0   component 0   time level 0
# column format: 1:it	2:tl 3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
# data columns: 13:qlm_mp_m0[0] 14:qlm_mp_m1[0] 15:qlm_mp_m2[0] 16:qlm_mp_m3[0] 17:qlm_mp_m4[0] 18:qlm_mp_m5[0] 19:qlm_mp_m6[0] 20:qlm_mp_m7[0] 21:qlm_mp_m8[0] 22:qlm_mp_j0[0] 23:qlm_mp_j1[0] 24:qlm_mp_j2[0] 25:qlm_mp_j3[0] 26:qlm_mp_j4[0] 27:qlm_mp_j5[0] 28:qlm_mp_j6[0] 29:qlm_mp_j7[0] 30:qlm_mp_j8[0]
3	0 0 0 0	0 0 0	0.0375	0 0 0	-0.78482780363911 -9.61504822243874e-16 0.394408453370077 -9.400606649521e-15 0.20172772719056 2.04885002755489e-14 0.258009960124327 -1.35434292724905e-13 0.455494975183582 2.16243956256747e-18 -2.58868367165937e-19 -6.26857009849198e-19 3.37019755087524e-18 -2.14965887939775e-18 -7.35023332474828e-18 -3.88192096564698e-19 -1.96752463770092e-17 -2.30129908458274e-17

